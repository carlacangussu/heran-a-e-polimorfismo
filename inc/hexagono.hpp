#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Hexagono : public FormaGeometrica{

public:
  Hexagono();
  Hexagono(float lado);
  ~Hexagono();

  void setLado(float lado);
  float getLado();

  void setTipo(string tipo);
  string getTipo();


float calculaArea();
float calculaPerimetro();

};
#endif
