#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Triangulo : public FormaGeometrica{

public:
  Triangulo();
  Triangulo(float base, float altura);
  ~Triangulo();


  void setBase(float base);
  float getBase();

  void setAltura(float altura);
  float getAltura();

  void setTipo(string tipo);
  string getTipo();

float calculaArea();
float calculaPerimetro();

};
#endif
