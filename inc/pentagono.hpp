#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica {

public:
  Pentagono();
  Pentagono(float lado, float apotema);
  ~Pentagono();

void setLado(float lado);
float getLado();

void setApotema(float apotema);
float getApotema();

void setTipo(string tipo);
string getTipo();


float calculaArea();
float calculaPerimetro();

};
#endif
