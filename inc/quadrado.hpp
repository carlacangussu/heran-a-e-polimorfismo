#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica{

public:
  Quadrado();
  Quadrado(float lado);
  ~Quadrado();

  void setLado(float lado);
  float getLado();

  void setTipo(string tipo);
  string getTipo();

float calculaArea();
float calculaPerimetro();

};
#endif
