#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Circulo : public FormaGeometrica{

public:
  Circulo();
  Circulo(float raio);
  ~Circulo();

  void setRaio(float raio);
  float getRaio();

  void setTipo(string tipo);
  string getTipo();

float calculaArea();
float calculaPerimetro();

};
#endif
