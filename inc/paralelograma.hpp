#ifndef PARALELOGRAMA_HPP
#define PARALELOGRAMA_HPP

#include <string>
#include"formageometrica.hpp"

using namespace std;

class Paralelograma : public FormaGeometrica {

public:
  Paralelograma();
  Paralelograma(float base, float lado, float altura);
  ~Paralelograma();

void setBase(float base);
float getBase();

void setLado(float lado);
float getLado();

void setAltura(float altura);
float getAltura();

void setTipo(string tipo);
string getTipo();

float calculaArea();
float calculaPerimetro();

};
#endif
