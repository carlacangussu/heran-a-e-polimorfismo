#include "triangulo.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Triangulo::Triangulo(){
	setBase(7.0);
	setAltura(4.0);
	setTipo("Triângulo");
}
Triangulo::Triangulo(float base, float altura){
	setBase(base);
	setAltura(altura);
	setTipo("Triângulo");
}
Triangulo::~Triangulo(){
	cout << "Destrutor do triângulo" << endl;
}
// Sobrescrita de métodos
float Triangulo::calculaArea(){
	return getBase()*getAltura()/2;
}
float Triangulo::calculaPerimetro(){
	float h;
	h = sqrt(pow(getBase(),2)+ pow(getAltura(),2));
	return h+getBase()+getAltura();
}
