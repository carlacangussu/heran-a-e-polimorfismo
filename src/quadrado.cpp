#include "quadrado.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Quadrado::Quadrado(){
	setLado(4.0);
	setTipo("Quadrado");
}
Quadrado::Quadrado(float lado){
	setLado(lado);
	setTipo("Quadrado");
}
Quadrado::~Quadrado(){
	cout << "Destrutor do Quadrado" << endl;
}
// Sobrescrita de métodos
float Quadrado::calculaArea(){
	return getLado()*getLado();
}
float Quadrado::calculaPerimetro(){

	return getLado()*4;
}
