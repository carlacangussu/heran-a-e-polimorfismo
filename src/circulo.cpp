#include "circulo.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Circulo::Circulo(){
	setRaio(5.0);
	setTipo("Círculo");
}
Circulo::Circulo(float raio){
	setRaio(raio);
	setTipo("Círculo");
}
Circulo::~Circulo(){
	cout << "Destrutor do círculo" << endl;
}
// Sobrescrita de métodos
float Circulo::calculaArea(){
	return getRaio()*(2*3.14);
}
float Circulo::calculaPerimetro(){
	float h;
	h = (getRaio()*2)*3.14;
	return h;
}
