#include "hexagono.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Hexagono::Hexagono(){
	setLado(3.0);
	setTipo("Hexágono");
}
Hexagono::Hexagono(float lado){
	setLado(lado);
	setTipo("Hexágono");
}
Hexagono::~Hexagono(){
	cout << "Destrutor do hexágono" << endl;
}
// Sobrescrita de métodos
float Hexagono::calculaArea(){

	return (3*(pow(getLado(),2))*sqrt(3))/2;
}
float Hexagono::calculaPerimetro(){

	return getLado()*6;
}
