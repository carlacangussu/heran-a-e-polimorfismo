#include "paralelograma.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Paralelograma::Paralelograma(){
	setBase(6.0);
  setLado(4.5);
	setAltura(3.0);
	setTipo("Paralelogramo");
}
Paralelograma::Paralelograma(float base, float lado, float altura){
	setBase(base);
  setLado(lado);
	setAltura(altura);
	setTipo("Paralelogramo");
}
Paralelograma::~Paralelograma(){
	cout << "Destrutor do paralelograma" << endl;
}
// Sobrescrita de métodos
float Paralelograma::calculaArea(){
	return getBase()*getAltura();
}
float Paralelograma::calculaPerimetro(){
	return (2*(getBase()+getLado()));
}
