#include "formageometrica.hpp"
#include <iostream>
#include "triangulo.hpp"
#include "circulo.hpp"
#include "hexagono.hpp"
#include "paralelograma.hpp"
#include "pentagono.hpp"
#include "quadrado.hpp"

using namespace std;

int main(int argc, char ** argv){

	FormaGeometrica forma1;
	FormaGeometrica * forma2 = new FormaGeometrica(8.0, 4.5);

	FormaGeometrica forma3(4.6, 9.2);

	cout << "Forma1: " << forma1.getTipo() << " área: " << forma1.calculaArea() << endl;
  cout << "Forma2: " << forma2->getTipo() << " área: " << forma2->calculaArea() << endl;
	cout << "Forma3: " << forma3.getTipo() << " área: " << forma3.calculaArea() << endl;

	delete forma2;

	Triangulo triangulo1(4.2, 5.7);
cout << "Triângulo1: " << triangulo1.getTipo() << " área: " << triangulo1.calculaArea() << " Perímetro: " << triangulo1.calculaPerimetro() << endl;

	Circulo circulo1;
	cout << "Círculo1: " << circulo1.getTipo() << " área: " << circulo1.calculaArea() << " Perímetro: " << circulo1.calculaPerimetro() << endl;

	Hexagono hexagono1;
	cout << "Hexágono1: " << hexagono1.getTipo() << " área: " << hexagono1.calculaArea() << " Perímetro: " << hexagono1.calculaPerimetro() << endl;

	Paralelograma paralelograma1;
cout << "Paralelogramo1: " << paralelograma1.getTipo() << " área: " << paralelograma1.calculaArea() << " Perímetro: " << paralelograma1.calculaPerimetro() << endl;

	Pentagono pentagono1;
	cout << "Pentágono1: " << pentagono1.getTipo() << " área: " << pentagono1.calculaArea() << " Perímetro: " << pentagono1.calculaPerimetro() << endl;

	Quadrado quadrado1;
	cout << "Quadrado1: " << quadrado1.getTipo() << " área: " << quadrado1.calculaArea() << " Perímetro: " << quadrado1.calculaPerimetro() << endl;

	return 0;

}
