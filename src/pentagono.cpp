#include "pentagono.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Pentagono::Pentagono(){
	setLado(3.5);
	setApotema(4);
	setTipo("Pentágono");
}
Pentagono::Pentagono(float lado, float apotema){
	setLado(lado);
	setApotema(apotema);
	setTipo("Pentágono");
}
Pentagono::~Pentagono(){
	cout << "Destrutor do Pentágono" << endl;
}
 //Sobrescrita de métodos

float Pentagono::calculaPerimetro(){
 float perimetro;
 perimetro = getLado()*5;
	return perimetro;
}

float Pentagono::calculaArea(){
 float area;
 area = ((getLado()*5) * getApotema())/2;
	return area;
}
